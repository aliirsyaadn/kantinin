from django.urls import path
from . import views

app_name = 'addMakanan'

urlpatterns = [
    path('', views.addMakanan, name='addMakanan'),
    path('success', views.success, name='success'),
]
