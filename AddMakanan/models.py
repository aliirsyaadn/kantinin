import os
from django.db import models

# Create your models here.
Kantin = (
    ('counter1','Counter1'),
    ('counter2','Counter2'),
    ('counter3','Counter3'),
    ('counter4','Counter4'),
    ('counter5','Counter5'),
    ('counter6','Counter6'),
    ('counter7','Counter7'),
    ('counter8','Counter8'),
    ('counter9','Counter9'),
    ('counter10','Counter10'),
    ('counter11','Counter11'),
    ('counter12','Counter12'),
    ('counter13','Counter13'),
    ('counter14','Counter14'),
    ('counter15','Counter15'),
    ('counter16','Counter16'),
    ('counter17','Counter17'),
    ('counter18','Counter18'),
    ('counter19','Counter19'),
    ('counter20','Counter20'),
    ('counter21','Counter21'),
    ('counter22','Counter22'),
)

Jenis_Makanan = (
    ('snack','Snack'),
    ('maincourse','Main Course'),
    ('dessert','Dessert'),
    ('beverages','Beverages'),
)

class Makanan(models.Model):
    kantin = models.CharField(max_length = 20, choices = Kantin)
    nama = models.CharField(max_length = 50)
    harga = models.IntegerField(default = 0)
    jenisMakanan = models.CharField(max_length = 20, choices = Jenis_Makanan)
    gambarMakanan = models.ImageField(blank = True, null = True, upload_to = "makananImage/%Y/%m/%D/", default ="icon.png")





