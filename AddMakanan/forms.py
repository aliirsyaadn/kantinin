from django import forms
from .models import Makanan

class MakananForm(forms.ModelForm):
    class Meta:
        model = Makanan
        fields = ('kantin','nama','harga','jenisMakanan', 'gambarMakanan')
        labels = {
            'kantin'        : "Kantin              :",
            'nama'          : "Nama Makanan        :",
            'harga'         : "Harga               :",
            'jenisMakanan'  : "Jenis Makanan       :",
            'gambarMakanan' : "Foto Makanan        :",
        }
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
