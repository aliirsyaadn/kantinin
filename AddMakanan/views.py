from django.shortcuts import render, redirect
from .models import Makanan
from .forms import MakananForm

# Create your views here.

def addMakanan(request):
    if request.method == 'POST':
        form = MakananForm(request.POST, request.FILES)
        if form.is_valid(): 
            form.save()
            return redirect('addMakanan:success')
    else:
        form = MakananForm()
    return render(request, 'addMakanan.html', {'form' : form})

def success(request):
    makanan = Makanan.objects.all()
    return render(request, 'success.html', {'form' : makanan})