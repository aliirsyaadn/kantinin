from django.shortcuts import render, redirect
from .models import Feedback
from .forms import FeedbackForm

# Create your views here.
def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            # return redirect('Feedback:feedback')
    else:
        form = FeedbackForm()
    return render(request, 'feedback.html', {'form' : form})

def viewfeedback(request):
    data = Feedback.objects.all()
    return render(request, 'view-feedback.html', {'form' : data})