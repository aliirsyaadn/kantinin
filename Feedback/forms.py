from django import forms
from .models import Feedback

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['nama', 'pesan']
        labels = {'nama' : 'Nama', 'pesan' : 'Pesan'}
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text'}),
            'pesan' : forms.Textarea(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'size':100})
        }