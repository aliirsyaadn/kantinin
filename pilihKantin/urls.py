from django.urls import path
from . import views

app_name = 'pilihKantin'

urlpatterns = [
    path('', views.pilihKantin, name='pilihKantin'),
    path('home', views.home, name='home'),
]
