from django.shortcuts import render
from AddMakanan.models import Makanan

# Create your views here.

def is_valid_queryparam(param):
    return param != '' and param is not None

def pilihKantin(request):
    makanan = Makanan.objects.all()
    title_contains_query=request.GET.get('title_contains')
    if is_valid_queryparam(title_contains_query):
        makanan = makanan.filter(nama__icontains=title_contains_query)

    return render(request, 'pilihKantinHome.html', {'form' : makanan})

def home(request):
    return render(request, 'pilihKantin.html')
